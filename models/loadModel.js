const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const loadScheme = new Schema(
    {
      name: {type: String},
      pickup_address: {type: String},
      delivery_address: {type: String},
      status: {type: String},
      state: {type: String},
      logs: [
        {type: new mongoose.Schema(
            {
              message: String,
            },
            {timestamps: {createdAt: 'created_at'}},
        )},
      ],
      dimensions: {
        width: {type: Number},
        length: {type: Number},
        height: {type: Number},
      },
      payload: {type: Number},
      created_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
      },
      assigned_to: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
      },
      id_truck: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'trucks',
      },
    },
    {
      timestamps: true,
    },
);


module.exports = mongoose.model('loads', loadScheme);

