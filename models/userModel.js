const mongoose = require('mongoose');
const bcrypt = require('bcrypt');


const Schema = mongoose.Schema;

const userScheme = new Schema(
    {
      email: {type: String, required: true, unique: true},
      password: {type: String, required: true},
      role: {type: String, required: true},
    },
    {
      timestamps: true,
    },
);

userScheme.pre('save', function(next) {
  if (!this.isModified('password')) {
    return next();
  }
  this.password = bcrypt.hashSync(this.password, 10);
  next();
});


userScheme.methods.validatePassword = async function validatePassword(data) {
  return bcrypt.compare(data, this.password);
};

userScheme.pre('findOneAndUpdate', function(next) {
  this._update.password = bcrypt.hashSync(this._update.password, 10);
  next();
});


module.exports = mongoose.model('users', userScheme);
