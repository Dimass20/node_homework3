const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const truckScheme = new Schema(
    {
      status: {type: String},
      type: {type: String, required: true},
      created_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
      },
      assigned_to: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
      },
    },
    {
      timestamps: true,
    },
);


module.exports = mongoose.model('trucks', truckScheme);

