# Backend API

## How to use
Make sure you have Node.js and the npm installed
```
$ git clone https://gitlab.com/Dimass20/node_homework3
$ cd node_homework3 
$ npm install
$ npm start
```
And point your browser to http://localhost:3000

## Configuration
Create a .env file in the root directory of your project. Add environment-specific variables on new lines in the form of NAME = VALUE. For example

```
PORT=8080
JWT_SECRET=key
```

