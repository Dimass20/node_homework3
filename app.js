require('dotenv').config();

const express = require('express');
const mongoose = require("mongoose");
const morgan = require('morgan');
const PORT = process.env.PORT || 8080;
const app = express();
const userRouter = require('./routes/userRouter');
const authRouter = require('./routes/authRouter.js');
const truckRouter = require('./routes/truckRouter.js');
const loadRouter = require('./routes/loadRouter.js');

app.use(express.json());
app.use(morgan('tiny'));


app.use('/api/users', userRouter);
app.use('/api/auth', authRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);



mongoose.connect(process.env.dbURI,{
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
  useNewUrlParser: true,
  useFindAndModify: false,
}, function(err){
    if(err) return console.log(err);
  
    app.listen(PORT, () => {
      console.log(`Server started on port ${PORT}`);
    });
    
});




