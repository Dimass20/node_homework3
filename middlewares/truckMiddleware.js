const mongoose = require('mongoose');
const userModel = require('./../models/userModel');
const truckModel = require('./../models/truckModel');

exports.checkTypeTruck = (req, res, next) => {
  const {type} = req.body;
  const arrType = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];

  const foundType = arrType.find((el) => el === type);

  if (foundType == undefined) {
    return res.status(400).send({message: 'The type of truck is required'});
  }

  next();
};

exports.isValidIdOfTruck = (req, res, next) => {
  const isValidId = mongoose.Types.ObjectId.isValid(req.params.id);

  if (!isValidId) {
    return res.status(400).json({message: `Id of truck is not valid`});
  }

  next();
};

exports.isRoleDriver = async (req, res, next) => {
  const foundDriver = await userModel.findById(req.idUser);

  if (foundDriver == null) {
    return res.status(200).json({message: 'not found the Driver'});
  }

  if (foundDriver.role !== 'DRIVER') {
    return res
        .status(400)
        .json({message: 'forbidden! The user is not driver'});
  }

  next();
};

exports.checkStatusOLByTruck = async (req, res, next) => {
  try {
    const truck = await truckModel.findOne({
      $and: [{assigned_to: req.idUser}, {status: 'OL'}],
    });

    if (truck == null) {
      next();
    } else {
      return res.json({
        message: 'forbidden! The user has a truck of status `OL`',
      });
    }
  } catch (err) {
    return res.status(500).json(err);
  }
};
