require('dotenv').config();
const jwt = require('jsonwebtoken');
const userModel = require('../models/userModel');


exports.validUserDataRegister = function(req, res, next) {
  const {email, password, role} = req.body;

  if (password == null || email == null || role == null) {
    return res.status(400).json({
      'message': 'password, role and email are required',
    });
  }

  if (role != 'SHIPPER' && role != 'DRIVER') {
    return res.status(400).json({
      'message': 'role should be SHIPPER or DRIVER',
    });
  }

  next();
};

exports.validUserDataLogin = function(req, res, next) {
  const {email, password} = req.body;

  if (password == null || email == null) {
    return res.status(400).json({
      'message': 'password and email are required',
    });
  }
  next();
};


exports.validToken = (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res.status(400).json({message: `No JWT token found!`});
  }

  try {
    const [tokenType, token] = header.split(' ');
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.idUser = decoded.id;
    next();
  } catch (err) {
    return res.status(400).json({message: `No JWT token valid!`});
  }
};


exports.userExists = async (req, res, next) => {
  try {
    const user = await userModel.findById(req.idUser).exec();

    if (user == null) {
      return res.status(400).json({message: `The user does not exist`});
    }
  } catch (err) {
    return res.status(500).json(err);
  }
  next();
};
