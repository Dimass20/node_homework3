const mongoose = require('mongoose');
const userModel = require('./../models/userModel');
const loadModel = require('./../models/loadModel');

exports.isRoleShipper = async (req, res, next) => {
  const foundShipper = await userModel.findById(req.idUser);

  if (foundShipper == null) {
    return res.status(200).json({message: 'not found the Shipper'});
  }

  if (foundShipper.role !== 'SHIPPER') {
    return res
        .status(200)
        .json({message: 'forbidden! The user is not shipper'});
  }

  next();
};

exports.checkLoadData = (req, res, next) => {
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;

  if (name == null) {
    return res.status(400).json({message: 'name is required'});
  }

  if (payload == null || typeof payload !== 'number') {
    return res.status(400).json({message: 'payload is required'});
  }

  if (pickup_address == null) {
    return res.status(400).json({message: 'pickup_address is required'});
  }

  if (delivery_address == null) {
    return res.status(400).json({message: 'delivery_address is required'});
  }

  if (dimensions == null) {
    return res.status(400).json({message: 'dimensions is required'});
  }

  if (
    !dimensions.hasOwnProperty('width') ||
    typeof dimensions.width !== 'number'
  ) {
    return res.status(400).json({message: 'width is required'});
  }

  if (
    !dimensions.hasOwnProperty('length') ||
    typeof dimensions['length'] !== 'number'
  ) {
    return res.status(400).json({message: 'length is required'});
  }

  if (
    !dimensions.hasOwnProperty('height') ||
    typeof dimensions.height !== 'number'
  ) {
    return res.status(400).json({message: 'height is required'});
  }

  next();
};

exports.isValidIdOfLoad = async (req, res, next) => {
  const isValidId = mongoose.Types.ObjectId.isValid(req.params.idLoad);

  if (!isValidId) {
    return res.status(400).json({message: `Id of load is not valid`});
  }

  try {
    const load = await loadModel.findById(req.params.idLoad);

    if (load == null) {
      return res.status(400).json({message: `not found load by id`});
    }

    if (load.created_by != req.idUser) {
      return res
          .status(400)
          .json({message: `forbidden! You don't have permission`});
    }
  } catch (err) {
    return res.status(500).json(err);
  }

  next();
};
