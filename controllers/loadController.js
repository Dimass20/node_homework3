const loadModel = require('./../models/loadModel');
const truckModel = require('./../models/truckModel');
const userModel = require('./../models/userModel');

const arrTrucks = [
  {
    type: 'SPRINTER',
    length: 300,
    width: 250,
    height: 170,
    weight: 1700,
  },
  {
    type: 'SMALL STRAIGHT',
    length: 500,
    width: 250,
    height: 170,
    weight: 2500,
  },
  {
    type: 'LARGE STRAIGHT',
    length: 700,
    width: 350,
    height: 200,
    weight: 4000,
  },
];

const statusLoad = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
];

exports.createLoad = async (req, res) => {
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;

  try {
    const load = new loadModel({
      name: name,
      payload: payload,
      pickup_address: pickup_address,
      status: 'NEW',
      delivery_address: delivery_address,
      dimensions: dimensions,
      created_by: req.idUser,
    });
    await load.save();

    return res.json({message: 'Load created successfully'});
  } catch (err) {
    return res.status(500).json(err);
  }
};

exports.searchDriver = async (req, res) => {
  const {idLoad} = req.params;

  try {
    const load = await loadModel.findOneAndUpdate(
        {_id: idLoad, status: 'NEW'},
        {status: 'POSTED'},
    );

    if (load == null) {
      return res.json({message: 'not found a load', driver_found: false});
    }

    const filterTrucks = arrTrucks.reduce((acum, el) => {
      if (
        el['length'] > load.dimensions['length'] &&
        el['width'] > load.dimensions['width'] &&
        el['height'] > load.dimensions['height'] &&
        el['weight'] > load['payload']
      ) {
        acum.push({type: el.type});
      }
      return acum;
    }, []);

    if (filterTrucks.length == 0) {
      await loadModel.findOneAndUpdate({_id: idLoad}, {status: 'NEW'});
      return res.json({
        message: 'Load posted successfully',
        driver_found: false,
      });
    }

    const driver = await truckModel.findOneAndUpdate(
        {
          $and: [{$or: [...filterTrucks]}, {status: 'IS', assigned_to:{ $ne: null} }],
        },
        {status: 'OL'},
    );

    if (driver == null) {
      await loadModel.findOneAndUpdate(
          {_id: idLoad},
          {status: 'NEW', $push: {logs: {message: `Load was not assigned`}}},
      );
      return res.json({
        message: 'Load posted successfully',
        driver_found: false,
      });
    }

    await loadModel.findOneAndUpdate(
        {_id: idLoad},
        {
          status: 'ASSIGNED',
          $push: {
            logs: {message: `Load assigned to driver with id ${driver._id}`},
          },
          state: 'En route to Pick Up',
          assigned_to: driver.assigned_to,
          id_truck: driver._id,
        },
    );

    return res.json({
      message: 'Load posted successfully',
      driver_found: true,
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json(err);
  }
};

exports.changeStatusOfLoad = async (req, res) => {
  const idUser = req.idUser;

  try {
    const load = await loadModel.findOne({
      assigned_to: idUser,
      status: 'ASSIGNED',
    });

    if (load == null) {
      return res.status(400).json({message: 'not found active load'});
    }

    const indexStatus = statusLoad.findIndex((el) => el === load.state);
    const newStatus =
      indexStatus == statusLoad.length - 1 ?
        statusLoad[indexStatus] :
        statusLoad[indexStatus + 1];
    await loadModel.findOneAndUpdate(
        {assigned_to: idUser, status: 'ASSIGNED'},
        {state: newStatus},
    );

    if (newStatus == statusLoad[statusLoad.length - 1]) {
      await loadModel.findOneAndUpdate(
          {assigned_to: idUser, status: 'ASSIGNED'},
          {status: 'SHIPPED'},
      );
      await truckModel.findOneAndUpdate(
          {assigned_to: idUser},
          {status: 'IS'},
      );
    }

    return res.json({message: `Load state changed to '${newStatus}'`});
  } catch (err) {
    return res.status(500).json(err);
  }
};

exports.updateLoad = async (req, res) => {
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;
  const idLoad = req.params.idLoad;

  try {
    await loadModel.findByIdAndUpdate(
        {_id: idLoad},
        {
          name: name,
          payload: payload,
          pickup_address: pickup_address,
          delivery_address: delivery_address,
          dimensions: dimensions,
        },
    );

    return res.json({message: 'Load details changed successfully'});
  } catch (err) {
    return res.status(500).json(err);
  }
};

exports.removeLoad = async (req, res) => {
  const idLoad = req.params.idLoad;

  try {
    await loadModel.findByIdAndRemove(idLoad);

    return res.json({message: 'Load deleted successfully'});
  } catch (err) {
    return res.status(500).json(err);
  }
};

exports.shipping_info = async (req, res) => {
  const idLoad = req.params.idLoad;
  const logs = [];

  try {
    const [load] = await loadModel
        .find({_id: idLoad})
        .populate({path: 'id_truck'});

    load.logs.forEach((el) => {
      logs.push({message: el.message, time: el.created_at});
    });

    return res.json({
      load: {
        _id: load._id,
        created_by: load.created_by,
        assigned_to: load.assigned_to,
        status: load.status,
        state: load.state,
        name: load.name,
        payload: load.payload,
        pickup_address: load.pickup_address,
        delivery_address: load.delivery_address,
        dimensions: {
          width: load.dimensions['width'],
          length: load.dimensions['length'],
          height: load.dimensions.height,
        },
        logs: [...logs],
        created_date: load.createdAt,
      },
      truck: {
        _id: load.id_truck._id,
        created_by: load.id_truck.created_by,
        assigned_to: load.id_truck.assigned_to,
        type: load.id_truck.type,
        status: load.id_truck.status,
        created_date: load.id_truck.createdAt,
      },
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json(err);
  }
};

exports.getActiveLoad = async (req, res) => {
  const idUser = req.idUser;
  const logs = [];

  try {
    const load = await loadModel.findOne({
      assigned_to: idUser,
      $or: [{status: 'ASSIGNED'}, {status: 'NEW'}, {status: 'POSTED'}],
    });

    if (load == null) return res.json({"load": {}});

    load.logs.forEach((el) => {
      logs.push({message: el.message, time: el.created_at});
    });

    return res.json({
      load: {
        _id: load._id,
        created_by: load.createdAt,
        assigned_to: load.assigned_to,
        status: load.status,
        state: load.state,
        name: load.name,
        payload: load.payload,
        pickup_address: load.pickup_address,
        delivery_address: load.delivery_address,
        dimensions: {
          width: load.dimensions['width'],
          length: load.dimensions['length'],
          height: load.dimensions.height,
        },
        logs: [...logs],
        created_date: load.createdAt,
      },
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json(err);
  }
};

exports.getListLoads = async (req, res) => {
  const idUser = req.idUser;
  let limit = req.query.limit;
  let offset = req.query.offset;
  const status = req.query.status;
  const arrLoads = [];
  const logs = [];
  let loads;

  limit = limit ? parseInt(limit, 10) : 10;
  offset = offset ? parseInt(offset, 10) : 0;
  limit = limit > 50 ? 50 : limit;

  try {
    const user = await userModel.findById(idUser);

    if (user.role === 'SHIPPER' && status) {
      loads = await loadModel
          .find({created_by: idUser, status: status})
          .skip(offset)
          .limit(limit);
    } else if (user.role === 'SHIPPER' && status == null) {
      loads = await loadModel
          .find({created_by: idUser})
          .skip(offset)
          .limit(limit);
    } else if (user.role === 'DRIVER' && status) {
      loads = await loadModel
          .find({assigned_to: idUser, status: status})
          .skip(offset)
          .limit(limit);
    } else if (user.role === 'DRIVER' && status == null) {
      loads = await loadModel
          .find({assigned_to: idUser})
          .skip(offset)
          .limit(limit);
    }

    loads.forEach((el) => {
      if (el) {
        el.logs.forEach((el2) =>
          logs.push({message: el2.message, time: el2.created_at}),
        );
      }
    });

    loads.forEach((el) => {
      if (el) {
        arrLoads.push({
          _id: el._id,
          created_by: el.created_by,
          assigned_to: el.assigned_to,
          status: el.status,
          state: el.state,
          name: el.name,
          payload: el.payload,
          pickup_address: el.pickup_address,
          delivery_address: el.delivery_address,
          dimensions: {
            width: el.dimensions['width'],
            length: el.dimensions['length'],
            height: el.dimensions.height,
          },
          logs: [...logs],
          created_date: el.createdAt,
        });
      }
    });

    return res.json({
      loads: [...arrLoads],
    });
  } catch (err) {
    console.log(err);
    return res.status(500).json(err);
  }
};

exports.getLoadById = async (req, res) => {
  const idLoad = req.params.idLoad;
  const logs = [];

  try {
    const load = await loadModel.findById(idLoad);

    if (load == null) return res.json({});

    load.logs.forEach((el) => {
      logs.push({message: el.message, time: el.created_at});
    });

    return res.json({
      load: {
        _id: load._id,
        created_by: load.createdAt,
        assigned_to: load.createdAt.assigned_to,
        status: load.createdAt.status,
        state: load.createdAt.state,
        name: load.createdAt.name,
        payload: load.createdAt.payload,
        pickup_address: load.createdAt.pickup_address,
        delivery_address: load.createdAt.delivery_address,
        dimensions: {
          width: load.dimensions['width'],
          length: load.dimensions['length'],
          height: load.dimensions.height,
        },
        logs: [...logs],
        created_date: load.createdAt,
      },
    });
  } catch (err) {
    return res.status(500).json(err);
  }
};
