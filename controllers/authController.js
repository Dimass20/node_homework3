require('dotenv').config();
const user = require('../models/userModel');
const jwt = require('jsonwebtoken');

exports.registerController = async function(req, res) {
  try {
    const newUser = user(req.body);
    await newUser.save();
    return res.status(200).json({message: 'Profile created successfully'});
  } catch (err) {
    if (err.code == '11000') {
      console.log(err);
      return res.status(400).json({message: 'username is already taken'});
    }

    return res.status(500).json({message: err});
  }
};

exports.loginController = async (req, res) => {
  const {email, password} = req.body;
  try {
    const foundUser = await user.findOne({email: email}).exec();

    if (!foundUser) {
      return res.status(400).send({message: 'The username does not exist'});
    }

    const match = await foundUser.validatePassword(password);

    if (!match) {
      return res.status(400).send({message: 'The password is invalid'});
    }

    const token = jwt.sign({id: foundUser._id}, process.env.JWT_SECRET);
    res.send({
      'jwt_token': token,
    });
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.changePsswrdController = async (req, res) => {
  const {email} = req.body;
  try {
    const foundUser = await user.findOne({email: email}).exec();

    if (!foundUser) {
      return res.status(400).send({message: 'The username does not exist'});
    }

    res.send({'message': 'New password sent to your email address'});
  } catch (error) {
    res.status(500).send(error);
  }
};
