const TruckModel = require('./../models/truckModel');

exports.addTruckController = async (req, res) => {
  const {type} = req.body;

  try {
    const truck = new TruckModel({type: type, created_by: req.idUser, status: 'IS'});
    await truck.save();
    return res.send({message: 'Truck created successfully'});
  } catch (err) {
    return res.status(500).json({message: err});
  }
};

exports.getTrucksByIdDriver = async (req, res) => {
  const arrTrucks = [];
  try {
    const trucks = await TruckModel.find({created_by: req.idUser}).exec();
    trucks.forEach((el) => {
      arrTrucks.push({
        _id: el._id,
        created_by: el.created_by,
        assigned_to: el.assigned_to,
        type: el.type,
        status: el.status,
        created_date: el.createdAt,
      });
    });

    return res.send({
      trucks: [...arrTrucks],
    });
  } catch (err) {
    return res.status(500).json({message: err});
  }
};

exports.getTruckByIdDriver = async (req, res) => {
  const idTruck = req.params['id'];

  try {
    const truck = await TruckModel.findById(idTruck).exec();

    if (truck == null || truck.created_by != req.idUser) {
      return res.status(400).json({message: 'not found'});
    }

    return res.send({
      truck: {
        _id: truck._id,
        created_by: truck.created_by,
        assigned_to: truck.assigned_to,
        type: truck.type,
        status: truck.status,
        created_date: truck.createdAt,
      },
    });
  } catch (err) {
    return res.status(500).json({message: err});
  }
};

exports.updateTruckByIdDriver = async (req, res) => {
  const idTruck = req.params['id'];
  const {type} = req.body;

  try {
    const truck = await TruckModel.findById(idTruck).exec();

    if (truck == null || truck.created_by != req.idUser) {
      return res.status(400).json({message: 'not found'});
    }

    await TruckModel.findOneAndUpdate({_id: idTruck}, {type: type});

    return res.send({message: 'Truck details changed successfully'});
  } catch (err) {
    return res.status(500).json({message: err});
  }
};

exports.assignTruckByIdDriver = async (req, res) => {
  const idTruck = req.params['id'];

  try {
    const truck = await TruckModel.findById(idTruck).exec();

    if (truck == null || truck.created_by != req.idUser) {
      return res.status(400).json({message: 'not found'});
    }

    await TruckModel.findOneAndUpdate(
        {_id: idTruck},
        {assigned_to: req.idUser, status: 'IS'},
    );

    return res.send({message: 'Truck assigned successfully'});
  } catch (err) {
    return res.status(500).json({message: err});
  }
};

exports.removeTruckById = async (req, res) => {
  const idTruck = req.params['id'];

  try {
    const truck = await TruckModel.findById(idTruck).exec();

    if (truck == null || truck.created_by != req.idUser) {
      return res.status(400).json({message: 'not found'});
    }

    await TruckModel.findOneAndDelete({_id: idTruck});

    return res.send({message: 'Truck deleted successfully'});
  } catch (err) {
    return res.status(500).json({message: err});
  }
};
