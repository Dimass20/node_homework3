const userModel = require('../models/userModel');

exports.getInfoUser = async (req, res) => {
  try {
    const [foundUser] = await userModel.find({_id: req.idUser});
    res.json({
      user: {
        _id: foundUser._id,
        email: foundUser.email,
        created_date: foundUser.createdAt,
      },
    });
  } catch (err) {
    res.status(500).json({err});
  }
};


exports.updatePsswrdUser = async (req, res) => {
  const {oldPassword, newPassword} = req.body;

  if (oldPassword == null || newPassword == null) {
    return res.status(400).json({
      'message': 'oldPassword and newPassword are required',
    });
  }

  try {
    const [foundUser] = await userModel.find({_id: req.idUser});

    const match = await foundUser.validatePassword(oldPassword);

    if (!match) {
      return res.status(400).send({message: 'The oldPassword is invalid'});
    }

    await userModel.findOneAndUpdate(
        {_id: req.idUser}, {password: newPassword});
    return res.send({'message': 'Password changed successfully'});
  } catch (err) {
    return res.status(500).send({'message': err});
  }
};


exports.deleteUser = async (req, res) => {
  try {
    await userModel.findByIdAndRemove(req.idUser);
    return res.send({'message': 'Profile deleted successfully'});
  } catch (err) {
    return res.status(500).send({'message': err});
  }
};

