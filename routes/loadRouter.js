const express = require('express');
const loadRouter = new express.Router();
const {validToken, userExists} = require('./../middlewares/userData');
const {
  createLoad,
  searchDriver,
  changeStatusOfLoad,
  updateLoad,
  removeLoad,
  shipping_info,
  getActiveLoad,
  getListLoads,
  getLoadById,
} = require('./../controllers/loadController');

const {
  isRoleShipper,
  checkLoadData,
  isValidIdOfLoad,
} = require('./../middlewares/loadMiddleware');

const {isRoleDriver} = require('./../middlewares/truckMiddleware');

loadRouter.post(
    '/',
    validToken,
    userExists,
    isRoleShipper,
    checkLoadData,
    createLoad,
);

loadRouter.post(
    '/:idLoad/post',
    validToken,
    userExists,
    isRoleShipper,
    isValidIdOfLoad,
    searchDriver,
);

loadRouter.patch(
    '/active/state',
    validToken,
    userExists,
    isRoleDriver,
    changeStatusOfLoad,
);

loadRouter.put(
    '/:idLoad',
    validToken,
    userExists,
    isRoleShipper,
    isValidIdOfLoad,
    checkLoadData,
    updateLoad,
);

loadRouter.delete(
    '/:idLoad',
    validToken,
    userExists,
    isRoleShipper,
    isValidIdOfLoad,
    removeLoad,
);

loadRouter.get(
    '/:idLoad/shipping_info',
    validToken,
    userExists,
    isRoleShipper,
    isValidIdOfLoad,
    shipping_info,
);

loadRouter.get('/active', validToken, userExists, isRoleDriver, getActiveLoad);
loadRouter.get('/', validToken, userExists, getListLoads);

loadRouter.get(
    '/:idLoad',
    validToken,
    userExists,
    isValidIdOfLoad,
    getLoadById,
);


module.exports = loadRouter;
