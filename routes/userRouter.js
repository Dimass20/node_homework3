const express = require('express');
const userRouter = new express.Router();
const {validToken, userExists} = require('./../middlewares/userData');
const {
  getInfoUser,
  updatePsswrdUser,
  deleteUser,
} = require('../controllers/userController');
const {checkStatusOLByTruck} = require('./../middlewares/truckMiddleware');


userRouter.get('/me', validToken, userExists, getInfoUser);
userRouter.patch('/me/password', validToken, userExists, updatePsswrdUser);
userRouter.delete(
    '/me',
    validToken,
    userExists,
    checkStatusOLByTruck,
    deleteUser,
);


module.exports = userRouter;
