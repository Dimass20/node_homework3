const express = require('express');
const authRouter = new express.Router();
const {validUserDataRegister, validUserDataLogin} = require('./../middlewares/userData');
const {
  registerController,
  loginController, changePsswrdController,
} = require('../controllers/authController');

authRouter.post('/register', validUserDataRegister, registerController);
authRouter.post('/login', validUserDataLogin, loginController);
authRouter.post('/forgot_password', changePsswrdController);


module.exports = authRouter;
