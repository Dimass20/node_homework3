const express = require('express');
const truckRouter = new express.Router();
const {validToken, userExists} = require('./../middlewares/userData');
const {
  addTruckController,
  getTrucksByIdDriver,
  getTruckByIdDriver,
  updateTruckByIdDriver,
  assignTruckByIdDriver,
  removeTruckById,
} = require('./../controllers/truckController');

const {
  checkTypeTruck,
  isValidIdOfTruck,
  isRoleDriver,
  checkStatusOLByTruck,
} = require('./../middlewares/truckMiddleware');


truckRouter.post(
    '/',
    validToken,
    userExists,
    isRoleDriver,
    checkTypeTruck,
    addTruckController,
);

truckRouter.get('/', validToken, userExists, isRoleDriver, getTrucksByIdDriver);

truckRouter.get(
    '/:id',
    validToken,
    userExists,
    isRoleDriver,
    isValidIdOfTruck,
    getTruckByIdDriver,
);

truckRouter.put(
    '/:id',
    validToken,
    userExists,
    isRoleDriver,
    isValidIdOfTruck,
    checkTypeTruck,
    checkStatusOLByTruck,
    updateTruckByIdDriver,
);

truckRouter.post(
    '/:id/assign',
    validToken,
    userExists,
    isRoleDriver,
    isValidIdOfTruck,
    checkStatusOLByTruck,
    assignTruckByIdDriver,
);

truckRouter.delete(
    '/:id',
    validToken,
    userExists,
    isRoleDriver,
    isValidIdOfTruck,
    checkStatusOLByTruck,
    removeTruckById,
);


module.exports = truckRouter;
